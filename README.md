# Subject #

This project is designed to build Stereo Photo View into redistributable packages for linux.

# Build AppImage

## Dependencies

The application image is built in a docker container, so the following applications must be installed:

* [Docker](https://docs.docker.com/get-docker/)
* [Docker Compose](https://docs.docker.com/compose/install/)

## Examples for build:

```
./build-appimage 1.15.2
./build-appimage develop
./build-appimage release/1.21.0
```

After that, the AppImage package should be located in the `./out` directory.

## Upload packet to the bitbucket

```
cd out
curl -v -u <user> -X POST https://api.bitbucket.org/2.0/repositories/stereophotoview/stereophotoview/downloads -F files=@<fileName>
```

## Clear docker

```
cd docker && \
sudo docker-compose rm -f && \
sudo docker rmi stereophotoview-build-appimage
```

# Launchpad package

## Update version

```
cd deb
./update-stereophotoview-deb 1.14.5 -1~focal 1.14.6 -1~focal
```

## Initial setup

Add to ~/.bashrc:
 
```
DEBEMAIL=email
DEBFULLNAME=FullName
export DEBEMAIL DEBFULLNAME
```
 
Preparing keys

All changes are signed using gpg, so you need to create a key:

```
gpg --full-generate-key
```

Enter the email address and name, as in the initial setup.
We get a fingerprint of the key:

```
gpg --list-keys
```

We publish the key on the server:

```
gpg --keyserver keyserver.ubuntu.com --send-keys <fingerprint>
```

We specify this fingerprint in the account settings on launchpad


Add dput config (~/.dput.cf), like this:

```
[ppa-stable]
fqdn = ppa.launchpad.net
method = ftp
incoming = ~stereophotoview/stable/ubuntu/
login = anonymous
allow_unsigned_uploads = 0
```
